
import 'package:contador/src/pages/contador-page.dart';
import 'package:flutter/material.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false, //para quitar el mensaje de debug en la pantalla
        home: Center(
        child: ContadorPage(),
    ));
  }
}
