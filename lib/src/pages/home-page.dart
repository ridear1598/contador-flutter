import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  final stiloTexto = new TextStyle(fontSize: 25);
  final conteo = 10;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Titulo'),
          centerTitle: true,
          elevation: 0.2,
        ),
        body: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Hola a todos',
              style: stiloTexto,
            ),
            Text(conteo.toString(), style: TextStyle(fontSize: 20)),
          ],
        )),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {
            print('Hola a todos');
            //conteo = conteo + 1;
          },
        ));
  }
}
