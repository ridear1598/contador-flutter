import 'package:flutter/material.dart';

class ContadorPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
     return _ContadorPageState();
  }
}

class _ContadorPageState extends State<ContadorPage> {
  final _stiloTexto = new TextStyle(fontSize: 25);
  int _conteo = 10;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('StatefulWidget'),
          centerTitle: true,
          elevation: 0.2,
        ),
        body: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Hola a todos',
              style: _stiloTexto,
            ),
            Text(_conteo.toString(), style: TextStyle(fontSize: 20)),
          ],
        )),

        floatingActionButton: _crearBotones()

        );
  }


  Widget _crearBotones(){
      return Row(
        mainAxisAlignment:MainAxisAlignment.end,
        children: <Widget>[
          SizedBox(width:30.0),
          FloatingActionButton( child: Icon(Icons.exposure_zero), onPressed:_reset),
          Expanded(child: SizedBox()),
          FloatingActionButton(onPressed:_quitar, child: Icon(Icons.remove)),
          SizedBox(width:30.0),
          FloatingActionButton(onPressed:_agregar, child: Icon(Icons.add)),

        ],
      );
  }
  void _agregar(){
    setState(() =>_conteo++);
  }

  void _quitar(){
    setState(() =>_conteo--);
  }

   void _reset(){
      setState(() =>_conteo=0);
   }
}
